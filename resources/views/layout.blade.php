<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description"
          content="The DC Comic Collector where you can make your own Comic Book collection list so that you can keep track of all your DC Comic Book Issues and read about the latest news from the world of DC Comics">
    <meta name="keywords"
          content="Laravel, Foundation, jQuery, DC Comics, Comic Book Collector, The Flash, Batman, Superman, Aquaman, News, Collection">
    <meta name="author" content="Peter Boersma">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:title" content="DC | Comic Collector"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description"
          content="The DC Comic Collector where you can make your own Comic Book collection list so that you can keep track of all your DC Comic Book Issues and read about the latest news from the world of DC Comics"/>
    <meta property="og:url" content="http://www.frox.lycan-media.nl/"/>
    <meta property="og:image" content="http://www.frox.lycan-media.nl/img/logo.png"/>

    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@frox.lycan-media.nl"/>
    <meta name="twitter:creator" content="@LycanMD"/>

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="img/favicons/android-icon-192x192.png">
    <link rel="mask-icon" href="img/favicons/favicon.svg" color="blue">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicons/favicon-16x16.png">
    <link rel="manifest" href="img/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title>DC - Comic Collector</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/select2-bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}"/>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

</head>
<body>

@include('partials/navigation')
@yield('content')

<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@10.19.0/dist/lazyload.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $(".selectHero").select2({
        theme: 'bootstrap4',
            ajax: {
                url: "http://dc.lycan-media.nl/api/comicsList",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data,
                        pagination: {
                            more: (params.page * 5) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Search for a comic',
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 0,
            templateResult: formatComic,
            templateSelection: formatRepoSelection
        });
        function formatComic(comic) {
            if (comic.loading) {
                return comic.series_title;
            }
            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='container-fluid'>" +
                "<div class='row'>" +
                "<div class='col-5 col-md-2'>" +
                "<img src='" + comic.cover_url + "' class='img-fluid'>" +
                "</div>" +
                "<div class='col-7'>" +
                "<p class='font-weight-bold mb-0'> " + comic.series_title + " #" + comic.issue + "</p>" +
                "<p class='text-muted mb-0' style='font-size: 12px'>Written by: " + comic.comic_writer + "</p>" +
                "<p class='text-muted mb-0' style='font-size: 12px'>Art by: " + comic.comic_illustrator + "</p>" +
                "<small class='text-muted' style='font-size: 12px'>Released: " + comic.release_date + "</small>" +
                "</div>" +
                "</div>" +
                "</div>";
            return markup;
        }
        function formatRepoSelection(comic) {
            return comic.text || comic.series_title + " #" + comic.issue;
        }

    $('.selectHero').on('select2:select', function (e) {
        var data = e.params.data;
        $('#addComicName').val(data.hero);
        $('#addComicID').val(data.id)
    });

</script>
</body>
</html>
