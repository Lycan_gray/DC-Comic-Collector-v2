@extends('layout')
@section('content')

    <div class="sortbyBar p-3">
        <div class="container-fluid">
            <form method="post">
                {{ csrf_field() }}
                <div class="row justify-content-end">
                    <div class="col-12 col-md-1 mb-2">
                        <input class="form-control mr-sm-2" value="{{$heroid}}" type="text" name="searchTerm" id="id"
                               placeholder="Search"
                               aria-label="Search">
                    </div>
                    <div class="col-6 col-md-1">
                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-primary active">
                                <input type="radio" name="order" value="ASC" id="option1" autocomplete="off"
                                       @if($order == "ASC") checked @else @endif><i
                                    class="fas fa-sort-numeric-up"></i>
                            </label>
                            <label class="btn btn-primary">
                                <input type="radio" name="order" value="DESC" id="option2" autocomplete="off"
                                       @if($order == "DESC") checked @else @endif ><i
                                    class="fas fa-sort-numeric-down"></i>
                            </label>
                        </div>
                    </div>
                    <div class="col-6 col-md-1">
                        <button class="btn btn-primary" type="submit">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container-fluid mt-5">
        <div class="row justify-content-between">
            <div class="col-7">
                @if(count($comics) == 0)
                    <h2>No results found for <strong>"{{$heroid}}"</strong></h2>
                @else
                    @if($heroid == "")
                        <h2>Your Comics (<span class="text-muted">{{count($comics)}}</span>)</h2>
                    @else
                        <h2>Results for: <span class="font-weight-bold">"{{$heroid}}"</span> <span
                                class="text-muted">({{count($comics)}})</span></h2>
                    @endif
            </div>
            <div class="col-4">
                <select name="comicAdd" class="selectHero w-100"></select>
            </div>
            <div class="col-1">
                <form method="post" action="/addToInventory" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input id="addComicName" type="hidden">
                    <input id="addComicID" name="addComicID" type="hidden">
                    <input type="submit" class="btn btn-primary" value="Add Comic">
                </form>
            </div>
        </div>
        <hr>
        <div class="row">
            @foreach ($comics as $comicInst)
                @foreach ($comicInst as $comic)
                    <div class="col-12 col-sm-6 col-md-3 col-lg-1 mb-2">
                        <img src="{{$comic -> coverLink}}" class="comic img-fluid"
                             alt="{{$comic -> hero . " #" . $comic -> issue}}">
                        <small class="font-weight-bold">{{$comic -> hero }} #{{$comic -> issue}}</small>
                    </div>
                @endforeach
            @endforeach
        </div>
        @endif
    </div>
@endsection

