<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class userInventory extends Model
    {
        protected $table = "userItems";
        protected $fillable = [
            'user_id', 'comic_id',
        ];
        public $timestamps = false;

    }
