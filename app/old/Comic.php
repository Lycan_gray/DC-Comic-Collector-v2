<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Comic extends Model
    {
        protected $table = "comic";
        protected $fillable = ['id', 'hero_id', 'coverLink', 'hero', 'issue', 'release', 'writer', 'illustrator', 'coverIllustrator'];

        public function test()
        {
            return "IK BEN ER";
        }
    }
