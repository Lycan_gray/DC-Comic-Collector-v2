<?php

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

//    Route::match(['get', 'post'], '/', 'CCollectorController@index')->middleware('auth');
//    Route::get('/search', 'CCollectorController@search')->middleware('auth');

    Auth::routes();

    Route::get('/', 'HomeController@index')->name('home');
    Route::match(['get', 'post'], '/inventory', 'HomeController@showGlobalInventory')->name('global_inventory');
    Route::match(['get', 'post'], '/profile/inventory', 'HomeController@showUserInventory')->name('user_inventory')->middleware('auth');


    Route::get('/admin', 'AdminController@admin')
        ->middleware('is_admin')
        ->name('admin');


    Route::post('/addToInventory', 'HomeController@addToInventory')->middleware('auth');

    Route::post('/createComic', 'HomeController@store')->middleware('auth');
    Route::post('/createHero', 'HomeController@store')->middleware('auth');

